const { expect } = require('chai')
const request = require('supertest')

// App
const app = require('../app')
const agent = request.agent(app)

// Test suite
describe('API', () => {
  it('should say `Hello Hackathon`', async () => {
    const res = await request(app).get('/api')
    expect(res.status).to.equal(200)
    expect(res.text).to.be.a('string')
    expect(res.text).to.equal('Hello Hackathon')
  })

  it('should echo a query', done => {
    agent
      .get('/api')
      .query({ q: 'echo' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('echo')
        done()
      })
  })

  //   it('should say my name', done => {
  //     agent
  //       .get('/api')
  //       .query({ q: 'e03cdd70: what is your name' })
  //       .expect('Content-Type', /text/)
  //       .expect(200)
  //       .end((err, res) => {
  //         if (err) return done(err)
  //         expect(res.text).to.be.a('string')
  //         expect(res.text).to.equal('mattx')
  //         done()
  //       })
  //   })

  it('Test', done => {
    agent
      .get('/api')
      .query({ q: '/what is/' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('yes')
        done()
      })
  })

  context('should error', () => {
    it('on request to a non-existing endpoint', done => {
      agent
        .get('/api/bla')
        .expect(404)
        .end(done)
    })
  })
})
