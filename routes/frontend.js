const { Router } = require('express')
const router = new Router()

// Controller
const frontendController = require('../controllers/frontend')

const frontendRouter = () => {
  router.route('/').get(frontendController.home)
  return router
}

module.exports = frontendRouter
