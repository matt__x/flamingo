/* eslint-disable prettier/prettier */
module.exports = {
  hackathon: (req, res) => {
    let query = req.query
    let question = req.query.q
    let answer //= question || 'Hello Hackathon'

    if (/what is your name/.test(question)) {
      answer = 'mattx'
    } else {
      answer = question || 'Hello Hackathon'
    }

    if (/what is/.test(question)) {
      if (question.includes('what')) {
        answer = 'yes'
      }
    } else {
      answer = question || 'Hello Hackathon'
    }

    // Log a query from game server
    if (process.env.NODE_ENV != 'test') console.log(query) // eslint-disable-line no-console

    // Answer the question
    res.status(200).send(String(answer))
  }
}
